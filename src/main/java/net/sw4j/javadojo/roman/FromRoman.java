package net.sw4j.javadojo.roman;

/**
 * A class with a method to convert a Roman numeral into an integer.
 */
public class FromRoman {

    /**
     * Converts the given {@code romanValue} in the range from {@code I}
     * ({@code 1}) to {@code MMMCMXCIX} ({@code 3999}) into an integer.
     *
     * @param romanValue the {@code value} to convert.
     * @return the integer.
     */
    public int fromRoman(String romanValue) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
